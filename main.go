package main

import (
	"bufio"
	"github.com/rs/zerolog"
	"github.com/urfave/cli/v2"
	"log"
	"os"
)

func main() {
	app := &cli.App{
		Name:  "zl-pretty",
		Usage: "pretty-formats a zerolog-style JSON log from stdin to stdout",
		Action: func(c *cli.Context) error {
			writer := zerolog.ConsoleWriter{Out: os.Stdout}
			scanner := bufio.NewScanner(os.Stdin)
			for scanner.Scan() {
				writer.Write([]byte(scanner.Text()))
			}
			return scanner.Err()
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
