module gitlab.com/sam.osterkil/zl-pretty

go 1.14

require (
	github.com/urfave/cli/v2 v2.2.0
  github.com/rs/zerolog v1.19.0
)
