# zl-pretty

Just takes zerolog JSON input on stdin and outputs the pretty version.

```sh
> ./zl-emitting-tool | zl-pretty
12:04PM INF cmd/service/main.go:81 > Initialize open-telemetry service=data_service
12:04PM INF cmd/service/main.go:84 > Initialize start application trace service=data_service
12:04PM INF cmd/service/main.go:92 > Service data_service started. service=data_service
12:04PM DBG cmd/service/main.go:127 >  configuration={"serviceaddress":":9000"} service=data_service
12:04PM INF cmd/service/main.go:151 > Launching gRPC server on [::]:9000 service=data_service
```
